# <> Desafio Frontend Multitech </>

## Desafio

Baseado no layout abaixo,desenvolva uma página de login seguindo os assets oferecidos. ![Layout](/assets/images/login-page-without-validation.png) 

### Teste para FrontEnd 

  
Objetivo deste teste é avaliar seus conhecimentos em organização, estilo, boas práticas e habilidades em front end.

  
### Requisitos

  
* HTML

* CSS (pré-processador a sua escolha)

* JS (fique livre para utilizar um framework)

* Desenvolver a página conforme o layout apresentado;

* Suporte para IE9+, Chrome, Safari, Firefox+ :)

* Responsivo;


### O que apreciamos ++

* Feedbacks visuais para o usuário (alertas, inputs...);

* CSS bem estruturado;

* Código limpo e bem organizado;

* HTML semántico;

### Diferencial (Consumindo API) 
* Simular um login autenticando e buscando os dados do usuário atráves do endpoint https://62681ad33f45bffa83879c74.mockapi.io/api/v1/authentication.

* Após logar, listar os produtos retornados atráves do endpoint https://62681ad33f45bffa83879c74.mockapi.io/api/v1/products

### Quem buscamos

Queremos uma pessoa que gosta do que faz, que trabalhe em equipe e tenha vontade de inovar. Sempre buscando atualização e soluções inovadoras.

Se você se identificou, venha fazer parte do nosso time!

### E ai preparado?

Faça o teste usando a branch `development` assim que finalizar abra uma MERGE REQUEST para a `main`.
Feito isso responda o e-mail avisando que finalizou. Desta forma vamos analisar o seu projeto pela MERGE REQUEST.

E ai curtiu? #GOMULTI
